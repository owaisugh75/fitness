@extends('layouts/email-template-new')
@section('heading')
    Invitation from fitness
@stop

@section('name')
    {{ $data['first_name'] }},
@stop

@section('statement')
    <p>We are happy to inform you that you are invited by <span style="font-weight: bold; color:#3e3e3e;"> {{ $data['vendor_name'] }} </span> to join <span style="font-weight: bold; color:#00B7FD;"> fitness</span>.</p>
    <p>Your registration details are given below, contact your associated vendor if there are any changes to the same.</p>
    <table>
        <tr style='padding:5px;text-align: left'><th>Login details</th><tr>
        <tr style='padding:3px;text-decoration:none !important'><td> Username: <b>{{ $data['email'] }} </b></td></tr>
    </table>

    <h4>Change your password</h4>

    <p><a href="{{ $data['url'] }}?token={{ $data['email_token'] }}"
            style="
                    background: #00B7FD;
                    border: 1px solid #00B7FD;
                    color: #fff;
                    font-weight: bold;
                    text-transform: uppercase;
                    padding: 9px 12px;
                    font-size: 15px;
                    border-radius: 4px;
                    margin-bottom: 5px;
                    display:inline-block;
                    text-decoration:none;">
            Change my password
        </a>
    </p>
    <p>or copy and paste this link into your browser:</p>
    <p style="text-align: left;">
        <a href="{{ $data['url'] }}?token={{ $data['email_token'] }}"> 
            {{ $data['url'] }}?token={{ $data['email_token'] }}
        </a>
    </p>
    
    <p>Note: Please note that this link will expire in 30 minutes.You may ignore this message if you haven't subscribed to our services.
    Hoping to see you soon.</p>
@stop