@extends('layouts/email-template-new')
@section('heading')
    fitness Password Reset Request
@stop

@section('name')
    {{ $data['first_name'] }},
@stop

@section('statement')

    <p>We have received a request to reset your fitness account password associated with this email address.</p>
    
    <p><a href="{{ $data['url'] }}?token={{ $data['email_token'] }}&user_type={{ $data['user_type']}}"
            style="
                background: #00B7FD;
                border: 1px solid #00B7FD;
                color: #fff;
                font-weight: bold;
                text-transform: uppercase;
                padding: 9px 12px;
                font-size: 15px;
                border-radius: 4px;
                margin-bottom: 5px;
                display:inline-block;
                text-decoration:none;">
            Change my password
        </a>
    </p>
    
    <p>or copy and paste this link into your browser:</p>

    <p style="text-align: left;">
        <a href="{{ $data['url'] }}?token={{ $data['email_token'] }}&user_type={{ $data['user_type'] }}"> 
        {{ $data['url'] }}?token={{ $data['email_token'] }}&user_type={{ $data['user_type']}}
    </p>
    </a>
    <p>If you didn't make this request, please ignore this email.
        Your password won't change until you access the link above and create a new one.</p>

    <p>Note: Please note that this link will expire in 30 minutes.You may ignore this message if you haven't subscribed to our services.
    Hoping to see you soon.</p>
@stop