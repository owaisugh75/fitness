@extends('layouts/email-template-new')
@section('heading')
  Password reset successful
@stop

@section('name')
  {{ $data['first_name'] }},
@stop

@section('statement')
    Your fitness account password has been successfully reset.
@stop