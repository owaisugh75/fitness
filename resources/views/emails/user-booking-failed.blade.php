@extends('layouts/order-email-template')
@section('heading')
    Payment Failed
@stop

@section('customer_name')
    {{ $data['enduser_name'] }},
@stop

@section('booking_statement')
    Oh no, this is frustrating, we get it. Unfortunately, your appointment 
    <span style="font-weight: bold; color:#00B7FD;"> #{{$data['booking_number']}} </span> 
    with <span style="font-weight: bold; color:#3e3e3e;"> {{$data['vendor_name']}} </span> as been failed
    due to an issue with your payment.Please check your credit/debit card informations and retry.   

@stop
