@extends('layouts/email-template')
@section('content')
Hi {{ucfirst($data['first_name'])}},
<p>We are happy to inform you that you are invited to join fitness.</p>
<p>Your registration details are given below, contact fitness Staff to change if there are any inconsistencies.</p>
<p> Login username: {{ $data['email'] }} </p>
@if ($data['contact_number'] != null)
	<p> Contact Number: {{ $data['contact_number'] }}</p>
@endif

@if ($data['hasPassword']) 
	<p>A custom password has been set by the administrator which shall be relayed to you in person/ via mobile, which can be used to login.</p> 
@else
<p>Please use the link below to set your password and ensure that it is shared with no one whatsoever.<br>
<a href="{{ $data['url'] }}">Secure Account</a></p>
@endif
@stop