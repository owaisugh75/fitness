
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="background: #f0f5f8; height:100%">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="icon" type="image/x-icon" href="{{ asset('images/appicon.png') }}">
        <style>
            html, body{height:100%;}
            body{display:flex; align-items:center; justify-content:center;}
            .login-outer{
                width:100%;
            }
            p {
                font-size: medium;
            }
            .h-login{
                margin-top: 0;
                font-size: 18px;
                color: #474747;
                font-weight: normal;
                border-bottom: 1px solid #eaeaea;
                padding-bottom: 14px;
            }
            .form-cover{
                display:flex;
                flex-wrap: wrap;
                justify-content: space-between;
            }
            .form-sub{
                width:100%;
            }
            .form-sub label{font-size:14px;}
            .form-sub input{
                display: block;
                padding: 9px 6px;
                margin-top: 7px;
                width:100%;
                box-sizing:border-box;
            }
            .info-txt{
                font-size:12px;
                font-style:italic;
                color:gray;
            }
            .btn.btn-primary{
                background: #3e3e3e;
                    border: 1px solid #3e3e3e;
                    color: #fff;
                    text-transform: uppercase;
                    font-weight: bold;
                    padding: 12px 25px;
                    border-radius: 3px;
                    margin-top: 8px;
            }
            .alert {
                position: relative;
                padding: .75rem 1.25rem;
                margin-bottom: 1rem;
                border: 1px solid transparent;
                border-radius: .25rem;
            }
            .alert-danger {
                color: #ca404e;
                background-color: #f8d7da;
                border-color: #f5c6cb;
            }
            .alert-success {
                color: #71c043;
                background-color: #d4edda;
                border-color: #c3e6cb;
            }
        </style>
    </head>
    <body style="background: #f0f5f8; " class="login-home">
        <div style="max-width:600px; width:100%; border:1px solid #d2d2d2; background:#fff; padding:30px; font-family:Arial, Helvetica, sans-serif; margin:0 auto; box-shadow:0 0 6px #c1d5e1; border-radius:6px">
<!--            {{-- <div class="" style="text-align:center; border-bottom:1px solid #f0f5f8; padding-bottom:15px; margin-bottom: 40px;"> --}}
                 <img src="{{ asset('images/fitness_logo.png') }}" width="250"> 
                {{-- <h2>Logo Here</h2>
            </div> --}}
            <div style="margin: -30px -30px 30px;text-align: center;background: #474747;padding: 3px 0;border-radius: 4px 4px 0 0;">
                <img src="{{ asset('images/logo.jpg') }}" alt="" style="width: 124px;" width="100">
            </div>-->
            <div class="login-outer">
                <span style="font-size:25px; color:#3e3e3e; margin-top:0; text-align: center; ">
                    @yield('heading')
                </span>

                        <h3 class="h-login">
                            @if ($data && empty($data['error']))
                                 Success!
                            @else
                                Something went Wrong!</div>
                            @endif
                        </h3>
                        <div class="form-cover">
                            <div class="form-sub">
                                
                                    @if ($data && empty($data['error']))
                                    <div class="alert alert-success">Email verified successfully!</div>
                                    @else
                                    <div class="alert alert-danger"> {{$data['error'] }}</div>
                                    @endif
                                
                            </div>
                        </div>                   
            </div>
           
            <!-- <div class="" style="text-align:center; border-top:1px solid #f0f5f8; padding-top:15px; margin-top: 40px;"> -->
              <!-- <p style="font-size: 12px; color:gray; margin:5px 0">&copy; 2020 MakingMeetings.Work all rights reserved.</p>  -->
            <!-- </div>/ -->
        </div>
    </body>
</html>
