@extends('layouts/order-email-template')
@section('heading')
    Booking Confirmation
@stop

@section('customer_name')
    {{ $data['vendor_name'] }},
@stop

@section('booking_statement')
    You have received a booking with <span style="font-weight: bold; color:#00B7FD;"> Booking ID: #{{$data['booking_number']}}</span>. See details below 
@stop

@section('info')
    <table style="background:#fff; color:#3e3e3e; width:100%; margin-top:8px">
        <tr>
            <td valign="top" width="70%">
                <span style="font-size: 17 px">Appoinment:</span><br/>
                <span style="color:gray; font-size: 22px; margin-bottom: 10px; display: block">
                    <span style="color:#00B7FD;">
                        {{$data['slot_start_for_email']}} - {{$data['slot_end_for_email']}}  ({{$data['duration']}} Hours)
                    </span> | <span style="color:#3e3e3e;"> {{$data['booking_date_for_email']}}
                    </span>
                </span>
                <span style="font-size: 17 px">Customer Name:</span><br/>
                <span style="color:#3e3e3e; font-size: 22px">
                    {{$data['enduser_name']}}
                </span>
            </td>
        </tr>
    </table>
@stop

@section('total_view')
    <td style="padding:5px 20px" >
        <h3 style="margin:0; color:#3e3e3e; text-align:right; font-weight: normal; font-size: 18px">Total</h3>
    </td>
    <td style="padding:5px 20px" width="100">
        <h2 style="margin:0; color:#3e3e3e;  text-align:right; ">$ {{ number_format($data['total'], 2) }} </h2>
    </td>
@stop