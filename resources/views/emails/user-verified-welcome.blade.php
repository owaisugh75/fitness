@extends('layouts/email-template-new')
@section('heading')
    Welcome to fitness
@stop

@section('name')
{{$user}},
@stop

@section('statement')
{{$user}}, We are happy to inform you that you are now a part of The fitness Team.
You may now access all of the services via our mobile apps.
@stop