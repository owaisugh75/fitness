@extends('layouts/email-template-new')
@section('heading')
    Confirm Your Email
@stop

@section('name')
    {{ $data['name'] }},
@stop

@section('statement')

    <p>Please click on the confirm button to verify your email address:</p>

    <p><a href="{{ $data['url'] }}?token={{ $data['uuid'] }}&email={{ $data['email'] }}"
            style="
                    background: #00B7FD;
                    border: 1px solid #00B7FD;
                    color: #fff;
                    font-weight: bold;
                    text-transform: uppercase;
                    padding: 9px 12px;
                    font-size: 15px;
                    border-radius: 4px;
                    margin-bottom: 5px;
                    display:inline-block;
                    text-decoration:none;">
            Confirm
        </a>
    </p>

    <p>or copy and paste this link into your browser:</p>
    <p style="text-align: left;">
        <a href="{{ $data['url'] }}?token={{ $data['uuid'] }}&email={{ $data['email'] }}"> 
        {{ $data['url'] }}?token={{ $data['uuid'] }}&email={{ $data['email'] }}
        </a>
    </p>
    
    <p>Note: Please note that this link will expire in 24 hours.You may ignore this message if you haven't subscribed to our services.
    Hoping to see you soon.</p>
@stop