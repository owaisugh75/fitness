@extends('layouts/order-email-template')
@section('heading')
    Booking Cancelled
@stop

@section('customer_name')
    {{ $data['vendor_name'] }},
@stop

@section('booking_statement')
The appointment <span style="font-weight: bold; color:#00B7FD;"> #{{$data['booking_number']}}</span>  with 
    <span style="font-weight: bold; color:#3e3e3e;"> {{$data['enduser_name']}}</span>
    has been cancelled. 
    
@stop

@section('info')
    <table style="background:#fff; color:#3e3e3e; width:100%, ">
        <tr>
            <td colspan="2">
                <h3 style="font-size:18; margin-bottom:0; margin-top:8px">
                    {{$data['vendor_name']}}
                </h3>
                
            </td>
        </tr>
        <tr>
            <td valign="top" width="70%">
                <span style="font-size: 17 px">Appoinment:</span><br/>
                <span style="color:gray; font-size: 19px; margin-bottom: 10px; display: block">
                    <span style="color:#00B7FD;">
                    {{$data['slot_start_for_email']}} - {{$data['slot_end_for_email']}} ({{$data['duration']}} Hours)
                    </span> | <span style="color:#3e3e3e;"> {{$data['booking_date']}}
                    </span>
                </span>
                <span style="font-size: 17 px">Contact Number:</span><br/>
                <span style="color:#3e3e3e; font-size: 22px">
                    {{$data['vendorDetails']['contact_number']}}
                </span>
            </td>
            <td>
                <span style="font-size: 17 px">Address:</span><br/>
                <span style="color:#3e3e3e; font-size: 17px">
                    {{$data['vendorDetails']['street1']}} <br/> {{$data['vendorDetails']['city']}} <br/> {{$data['state_name']}} <br/>USA
                </span>
            </td>
        </tr>
    </table>
@stop

@section('total_view')
    <td style="padding:5px 20px" >
        <h3 style="margin:0; color:#3e3e3e; text-align:right; font-weight: normal; font-size: 18px">Total</h3>
    </td>
    <td style="padding:5px 20px" width="100">
        <h2 style="margin:0; color:#3e3e3e;  text-align:right; ">$ {{ number_format($data['total'], 2) }} </h2>
    </td>
@stop