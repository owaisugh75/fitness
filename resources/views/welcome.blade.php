<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>fitness</title>
        <!-- Styles -->
        <style>
            img{
                position: absolute;
                right: 0;
                left: 0;
                margin: auto;
            }
        </style>

    </head>
    <div class="container">
        <body>
            <img class="centering" src="{{ asset('images/fitness_logo.png') }}">
        </body>
    </div>

    <style>
        .centering {
            position: absolute;
            top: 50%;
            bottom: 50%;
        }
    </style>
</html>
