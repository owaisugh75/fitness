<html lang="html">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Terms and Condtion</title>
    </head>
    <body style="font-family: Verdana, Geneva, Tahoma, sans-serif; line-height: 1.9; margin: 0; padding: 0; background-color: #f5f7fa;">
        <div style="width: 100%; background-color: #ffffff; max-width: 1200px; margin: 0 auto;">
            <div style="padding: 20px; max-width: 1200px; margin: 0 auto;">
            <img style="max-width: 80px;"  src="{{ asset('images/fitness_logo.png') }}">
            </div>
        </div>
        <div style="width: 100%; max-width: 1200px; margin: 0 auto; background-color: #ffffff; padding-top: 4px;" >
            <div style="padding: 20px;">
                <h2 style="margin-bottom: 20px; font-size: 1.5rem;">Terms and Condtion</h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore 
                    magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo 
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
                <p>
                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, 
                    eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam 
                    voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione 
                    voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci 
                    velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut 
                    enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi 
                    consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur,
                    vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
                </p>
                <p>
                    But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will 
                    give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, 
                    the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, 
                    but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful.
                </p>
            </div>
        </div>
    </body>
</html>