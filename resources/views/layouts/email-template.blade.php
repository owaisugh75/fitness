<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>[SUBJECT]</title>
    </head>
    <body>
        <div style="width:500px; border:1px solid #aaa; background:#eeeeee; padding:30px; font-family: neris; margin:0 auto;">
            <div class="" style="text-align:center; border-bottom:1px solid #f0f5f8; padding-bottom:15px; margin-bottom: 40px;">
                <img src="{{ asset('images/fitness_logo.png') }}" width="250">
            </div>
            <table style="background:#fff; padding:25px; color:#3e3e3e;width:100%">
                <tr>
                    @yield('content')
                </tr>
                <tr>
                    <b><p>Team fitness</p><b>
                </tr>
            </table>
        </div>
    </body>
</html>