<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="background: #f0f5f8">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>[SUBJECT]</title>
        <style>
            p {
                font-size: medium;
            }
        </style>
    </head>
    <body style="background: #f0f5f8; ">
        <div style="max-width:600px; width:100%; border:1px solid #d2d2d2; background:#fff; padding:30px; font-family:Arial, Helvetica, sans-serif; margin:0 auto; box-shadow:0 0 6px #c1d5e1; border-radius:6px">
            <div class="" style="text-align:center; border-bottom:1px solid #f0f5f8; padding-bottom:15px; margin-bottom: 40px;">
                <img src="{{ asset('images/fitness_logo.png') }}" width="250">
            </div>
            
            <table style="background:#fff; padding:0 0px; color:#3e3e3e; width:100%; color:#3e3e3e">
                <tr>
                    <td>
                        <h2 style="font-size:25px; color:#3e3e3e; margin-top:0; text-align: center; ">
                            @yield('heading')
                        </h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h3 style="margin-bottom: 0;">Hi @yield('name')
                        </h3>
                        <p style="font-size:16px">
                            @yield('statement')
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b><p>Team fitness</p><b>
                    </td>
                </tr>
            </table>
            <div class="" style="text-align:center; border-top:1px solid #f0f5f8; padding-top:15px; margin-top: 40px;">
              <p style="font-size: 12px; color:gray; margin:5px 0"> &copy; 2021 fitness all rights reserved.</p> 
            </div>
        </div>
    </body>
</html>