<?php

namespace App\Http\Middleware;

use Closure;
use Log;

class ValidateToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $userType = null)
    {
        $user = app('Dingo\Api\Auth\Auth')->user();
        if ($user->user_type == $userType && $user->status == 1) {
            return $next($request);
        }
        Log::info("Unauthorized endpoint");
        Log::info($user);
        return response()->json(['status' => 'Unauthorized endpoint', 'code' => 'blocked'], 401);

    }
}
